package excel;

import java.io.File;
import java.io.IOException;
import Exception.FileAlreadyExistsException;
import Exception.FileNotFoundException;
import java.util.ArrayList;
import jxl.*;
import jxl.write.*;
import outlook.Epost;

public class Excel {

    public void writeExcelFile(ArrayList<Epost> list, String fileName,
            boolean force)
            throws WriteException, IOException, FileAlreadyExistsException,
            FileNotFoundException {
        String[] columnNames = {
            "Datum   ",
            "Tid   ",
            "Transaktionsnummer",
            "Fordon  ",
            "Leverantör",
            "Hämtställe",
            "Artikelnamn                        ",
            "Artikelnummer",
            "Vikt 1   ",
            "Vikt 2   ",
            "Slutvikt   ",
            "Referens   "
        };
        Label[] label = new Label[columnNames.length];
        WritableWorkbook workbook;
        WritableSheet sheet;
        File file = new File(fileName);

        if (file.exists() && !force) {
            throw new FileAlreadyExistsException("Det finns redan en fil med"
                    + " det namnet! Vill du skriva över den?");
        }

        try {
            workbook = Workbook.createWorkbook(file);
            sheet = workbook.createSheet("Sheet1", 0);
//            sheet.setProtected(true);
        } catch (java.io.FileNotFoundException ex) {
            throw new FileNotFoundException("Filen är redan öppen i ett annat "
                    + "program, stäng det programmet och försök igen!");
        }

        for (int i = 0; i < columnNames.length; i++) {
            WritableCellFormat cell = new WritableCellFormat();
            cell.setLocked(true);
            label[i] = new Label(i, 0, columnNames[i]);
            sheet.addCell(new Label(i, 0, columnNames[i], cell));
        }

        for (int i = 0; i < list.size(); i++) {
            Epost emailList = list.get(i);

            WritableCellFormat cell = new WritableCellFormat();
            cell.setLocked(false);
            if (i - 1 >= 0) {
                if (checkDuplicates(emailList, list.get(i - 1))) {
                    cell.setBackground(Colour.RED);
                }
            }
            if (i + 1 < list.size()) {
                if (checkDuplicates(emailList, list.get(i + 1))) {
                    cell.setBackground(Colour.RED);
                }
            }
            for (int j = 0; j < columnNames.length; j++) {
                try {
                    sheet.addCell(new jxl.write.Number(j, i + 1, Double.valueOf(emailList.get(j)), cell));
                } catch (NumberFormatException ex) {
                    sheet.addCell(new Label(j, i + 1, emailList.get(j), cell));
                }

            }
            //    sheet.addCell(new Label(j, i + 1, emailList.get(j), cell));

        }

        for (int x = 0; x < columnNames.length; x++) {
            CellView cell = sheet.getColumnView(x);
            cell.setAutosize(true);
            sheet.setColumnView(x, cell);
        }

        workbook.write();
        workbook.close();
    }

    private boolean checkDuplicates(Epost e1, Epost e2) {
        if (e1.getDatum().equals(e2.getDatum()) && e1.getTid().equals(e2.getTid()) && e1.getLeverantor().equals(e2.getLeverantor())) {
            return true;
        } else {
            return false;
        }
    }

}
