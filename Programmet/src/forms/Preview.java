package forms;

import Exception.FileAlreadyExistsException;
import Exception.FileNotFoundException;
import Interface.FormInterface;
import excel.Excel;
import java.awt.Component;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import jxl.write.WriteException;
import outlook.Epost;
import programmet.Main;

/**
 *
 * @author Per
 */
public class Preview extends javax.swing.JPanel implements FormInterface {

    private ArrayList<Epost> epostArray;
    //   private final Excel excel = new Excel();
    private final JFrame frame;
    private final JDialog jd;

    /**
     * Skapar ett nytt formulär som används för förhandsgranskning.
     *
     * @param jd
     * @param frame
     */
    public Preview(JDialog jd, JFrame frame) {
        initComponents();
        this.frame = frame;
        this.jd = jd;
    }

    public void setList(ArrayList<Epost> lista) {
        this.epostArray = lista;
        String[] header = {
            "Datum", "Tid", "Transaktionsnummer", "Fordon", "Leverantör", "Hamtställe", "Artikelnamn", "Artikelnummer", "Vikt1", "Vikt2", "Slutvikt", "Referens"
        };
        String[][] array = new String[lista.size()][header.length];

        for (int i = 0; i < lista.size(); i++) {
            Epost epost = lista.get(i);

            array[i][0] = epost.getDatum();
            array[i][1] = epost.getTid();
            array[i][2] = epost.getTransaktionsNummer();
            array[i][3] = epost.getFordon();
            array[i][4] = epost.getLeverantor();
            array[i][5] = epost.getHamtStalle();
            array[i][6] = epost.getArtikelNamn();
            array[i][7] = epost.getArtikelNummer();
            array[i][8] = epost.getVikt1();
            array[i][9] = epost.getVikt2();
            array[i][10] = epost.getSlutVikt();
            array[i][11] = epost.getReferens();
        }
        jTable1.setModel(new DefaultTableModel(array, header));
        this.setTableSize();
    }

    private void setTableSize() {
        jTable1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        final TableColumnModel columnModel = jTable1.getColumnModel();
        for (int column = 0; column < jTable1.getColumnCount(); column++) {
            int width = 30; // Min width
            for (int row = 0; row < jTable1.getRowCount(); row++) {
                TableCellRenderer renderer = jTable1.getCellRenderer(row, column);
                Component comp = jTable1.prepareRenderer(renderer, row, column);
                width = Math.max(comp.getPreferredSize().width, width);
            }
            columnModel.getColumn(column).setPreferredWidth(width);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButtonSave = new javax.swing.JButton();
        jButtonCancel = new javax.swing.JButton();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null}
            },
            new String [] {
                "","","",""    }
        ));
        jScrollPane1.setViewportView(jTable1);

        jButtonSave.setText("Spara");
        jButtonSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveActionPerformed(evt);
            }
        });

        jButtonCancel.setText("Avbryt");
        jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1095, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonCancel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonSave)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 338, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonSave)
                    .addComponent(jButtonCancel)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelActionPerformed
        this.isDone();
    }//GEN-LAST:event_jButtonCancelActionPerformed

    private void jButtonSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveActionPerformed
        JFileChooser c = new JFileChooser();
        FileNameExtensionFilter xmlfilter = new FileNameExtensionFilter("Excel-datafil [.xls]", "xls");
        c.setDialogTitle("Spara Excel-datafil");
        c.setFileFilter(xmlfilter);

        int rVal = c.showSaveDialog(this);
        if (rVal == JFileChooser.APPROVE_OPTION) {
            Pattern p = Pattern.compile("[^a-z0-9åäö._-]", Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(c.getSelectedFile().getName());
            // Kolla om det finns otillåtna tecken i strängen.
            if (m.find()) {
                JOptionPane.showMessageDialog(null, "Otillåtna tecken i filnamnet!", "Problem", JOptionPane.ERROR_MESSAGE);
                return;
            }

            Excel excel = new Excel();
            String path = (c.getCurrentDirectory().toString() + "\\" + c.getSelectedFile().getName());
            if (!path.substring(path.length() - 4, path.length()).equalsIgnoreCase(".xls")) {
                path += ".xls";
            }
            this.saveToExcel(this.epostArray, excel, path, false);
            JOptionPane.showMessageDialog(null, "Filen har sparats utan problem.", "Problem", JOptionPane.INFORMATION_MESSAGE);
            this.isDone();
        }
    }//GEN-LAST:event_jButtonSaveActionPerformed

    private void saveToExcel(ArrayList<Epost> list, Excel excel, String path, boolean force) {
        try {
            excel.writeExcelFile(list, path, force);
        } catch (WriteException | IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileAlreadyExistsException ex) {
            String options[] = {"Ja", "Nej"};
            // Inverterat eversom svaret "ja" blir annars default
            if (JOptionPane.YES_OPTION == JOptionPane.showOptionDialog(this, ex.getMessage(), "Fil existerar redan.", 0, 0, null, options, options[1])) {
                this.saveToExcel(list, excel, path, true);
            }
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage());
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JButton jButtonSave;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables

    @Override
    public void isDone() {
        jd.setVisible(false);
        frame.toFront();
        frame.setEnabled(true);
    }
}
