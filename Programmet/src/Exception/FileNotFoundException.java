/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exception;

/**
 *
 * @author Per
 */
public class FileNotFoundException extends Exception {

    public FileNotFoundException(String text) {
        super(text);
    }
}
