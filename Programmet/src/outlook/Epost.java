package outlook;

/**
 *
 * @author Magnus
 */
public class Epost {

    // Variabel som innelagrarhåller hela det oformaterade mailet
    String epostKropp;
    // Variabler som lagrar mailets individiuella innehåll
    String datum;
    String tid;
    String transaktionsNummer;
    String fordon;
    String leverantor;
    String hamtStalle;
    String artikelNamn;
    String artikelNummer;
    String vikt1;
    String vikt2;
    String slutVikt;
    String referens;

    public String get(int id) {
        switch (id) {
            case 0:
                return datum;
            case 1:
                return tid;
            case 2:
                return transaktionsNummer;
            case 3:
                return fordon;
            case 4:
                return leverantor;
            case 5:
                return hamtStalle;
            case 6:
                return artikelNamn;
            case 7:
                return artikelNummer;
            case 8:
                return vikt1;
            case 9:
                return vikt2;
            case 10:
                return slutVikt;
            case 11:
                return referens;
        }
        return null;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getTransaktionsNummer() {
        return transaktionsNummer;
    }

    public void setTransaktionsNummer(String transaktionsNummer) {
        this.transaktionsNummer = transaktionsNummer;
    }

    public String getFordon() {
        return fordon;
    }

    public void setFordon(String fordon) {
        this.fordon = fordon;
    }

    public String getLeverantor() {
        return leverantor;
    }

    public void setLeverantor(String leverantor) {
        this.leverantor = leverantor;
    }

    public String getHamtStalle() {
        return hamtStalle;
    }

    public void setHamtStalle(String hamtStalle) {
        this.hamtStalle = hamtStalle;
    }

    public String getArtikelNamn() {
        return artikelNamn;
    }

    public void setArtikelNamn(String artikelNamn) {
        this.artikelNamn = artikelNamn;
    }

    public String getArtikelNummer() {
        return artikelNummer;
    }

    public void setArtikelNummer(String artikelNummer) {
        this.artikelNummer = artikelNummer;
    }

    public String getVikt1() {
        return vikt1;
    }

    public void setVikt1(String vikt1) {
        this.vikt1 = vikt1;
    }

    public String getVikt2() {
        return vikt2;
    }

    public void setVikt2(String vikt2) {
        this.vikt2 = vikt2;
    }

    public String getSlutVikt() {
        return slutVikt;
    }

    public void setSlutVikt(String slutVikt) {
        this.slutVikt = slutVikt;
    }

    public String getReferens() {
        return referens;
    }

    public void setReferens(String referens) {
        this.referens = referens;
    }

    public String getEpostKropp() {
        return epostKropp;
    }

    public void setEpostKropp(String epostKropp) {
        this.epostKropp = epostKropp;
    }

}
