package outlook;

import com.google.common.primitives.Ints;
import com.pff.PSTException;
import com.pff.PSTFile;
import com.pff.PSTFolder;
import com.pff.PSTMessage;
import database.SQLite;
import forms.Progress;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.apache.commons.codec.digest.DigestUtils;
import programmet.Main;
import settings.Settings;

/**
 *
 * @author Magnus
 */
public class OutlookReader implements Runnable {

    private boolean paused = true;
    SQLite sql;
    ArrayList<Epost> list = new ArrayList<>();
    ArrayList<Epost> komplettEpost = new ArrayList<>();
    ArrayList<SorteringsPaket> sorteringsLista = new ArrayList<>();

    private Progress progress;
    private Main frame;

    public OutlookReader(SQLite sql, Progress panel, Main frame) {
        this.sql = sql;
        this.progress = panel;
        this.frame = frame;
    }

    public OutlookReader() {

    }

    // Grunden för readEmails, går igenom ostfilen och samlar ihop alla data
    // Metoden returnerar en lång sträng utan radbryt, mellanslag etc.
    public ArrayList<Epost> getEmails(PSTFolder folder)
            throws PSTException, java.io.IOException {
        if (this.paused) {
            return null;
        }
        // Detta går igenom alla mappar och analyserar dem
        if (folder.hasSubfolders()) {
            Vector<PSTFolder> childFolders = folder.getSubFolders();
            for (PSTFolder childFolder : childFolders) {
                getEmails(childFolder);
            }
        }
        // Analyserar emails i Inkorgen
        this.progress.getJLabel1().setText("Steg 2 av 4 - Läser in email");
        if (folder.getContentCount() > 0) {
            PSTMessage email = (PSTMessage) folder.getNextChild();

            this.progress.getJLabel2().setText(Integer.toString(folder.getContentCount()));
            this.progress.getProgressBar().setMinimum(0);
            this.progress.getProgressBar().setMaximum(folder.getContentCount());

            int i = 0;
            for (int j = 0; j < folder.getContentCount(); j++) {
                if (this.paused) {
                    return null;
                }
                Epost epostObjekt = new Epost();

                this.progress.getJLabel3().setText(Integer.toString(i));
                this.progress.getProgressBar().setValue(i);

                // totalt 37963
                // Rensar epostet från radbryt, mellanslag, tabbar osv
                String epostKropp = email.getBody();
                epostKropp = epostKropp.replace("\n", "").replace("\r", "")
                        .replace(" ", "").replaceAll("\t", "");

                String epostStart = null;
                String epostStart2 = null;

                if (epostKropp.length() > 20) { // För att undvika string out of bounds exception
                    epostStart = epostKropp.substring(0, 6);
                    epostStart2 = epostKropp.substring(16, 20);
                    // Ser till att bara deponi-mail läggs till i listan

                    if (epostStart.equals("Datum:") && epostStart2.equals("Tid:")) {
                        epostObjekt.setEpostKropp(epostKropp);
                        i++;
                        list.add(epostObjekt);
                    }
                }
                email = (PSTMessage) folder.getNextChild();
            }

        }

        return list;
    }

    // Läser in epost från .pst med hjälp av 
    public ArrayList<Epost> readEmails(PSTFolder folder)
            throws PSTException, java.io.IOException {

        list.clear();
        getEmails(folder);
        return list;
    }

    // Denna metod används för att importera sorterade Epost-objekt direkt.
    public ArrayList<Epost> importEmails()
            throws PSTException, java.io.IOException {

        Settings.loadSettings();
        File file = new File(Settings.getFilePath());
        PSTFile pstFile = new PSTFile(file);
        ArrayList<Epost> allaEpost = readEmails(pstFile.getRootFolder());
        allaEpost = sortEmails(allaEpost);

        return allaEpost;
    }

    public void saveEmails()
            throws PSTException, java.io.IOException, Exception {
        //  ArrayList<Epost> saveList = importEmails();
        //  sql.saveToDatabase(saveList);
    }

    // Sorterar alla epoststrängar och lägger till objekt i en ArrayList som returneras.
    public ArrayList<Epost> sortEmails(ArrayList<Epost> list) throws IndexOutOfBoundsException {
        if (this.paused) {
            return null;
        }
        komplettEpost.clear();
        int lastTransaktionsNummer = sql.getLastTransaktionsNummer();
        //   System.out.println("Storlek av lista: " + list.size());
        this.progress.getJLabel1().setText("Steg 3 av 4 - Sorterar email");
        this.progress.getProgressBar().setMaximum(list.size());
        this.progress.getProgressBar().setValue(0);
        this.progress.getJLabel2().setText(Integer.toString(list.size()));
        int[] allTransaktionsNummer = sql.getAllTransaktionsNummer();

        for (int i = 0; i < list.size(); i++) {
            if (this.paused) {
                return null;
            }
            this.progress.getProgressBar().setValue(i);
            Epost epostObjekt = list.get(i);
            this.progress.getJLabel3().setText(Integer.toString(i));

            //     System.out.println("SORT;" + i);
            // Bryter ut alla relevanta värden ur strängen och lägger till dessa
            // i komplettEpost
            String epostKropp = epostObjekt.getEpostKropp();
            //      System.out.println(epostKropp);
            String epostDel = epostKropp.substring(6, 16);
            epostObjekt.setDatum(epostDel);

            epostKropp = epostObjekt.getEpostKropp();
            epostDel = epostKropp.substring(20, 28);
            epostObjekt.setTid(epostDel);

            int startVarde = 47;

            SorteringsPaket paket = new SorteringsPaket();
            paket.setStartVarde(startVarde);
            // Följande if-satser är tjuvlösningar. Måste fixas

            paket = sorter(epostObjekt.getEpostKropp(),
                    paket.getStartVarde(), 7, "Fordon:", paket);

            if (paket.getEpostDel().equals("TEST")) {
//                System.out.println("skippar test");
                continue;
            }
            //Troligen inte en bra långsiktig lösning
//            epostObjekt.setTransaktionsNummer(paket.getEpostDel());
            String fulLosning = paket.getEpostDel();
//            fulLosning = fulLosning.replaceAll("[^0-9.]", "");
            epostObjekt.setTransaktionsNummer(fulLosning);

            paket = sorter(epostObjekt.getEpostKropp(),
                    paket.getStartVarde(), 11, "Leverantör:", paket);
            epostObjekt.setFordon(paket.getEpostDel());

            if (epostKropp.contains("Hämtställe:")) {

                paket = sorter(epostObjekt.getEpostKropp(),
                        paket.getStartVarde(), 11, "Hämtställe:", paket);
                epostObjekt.setLeverantor(paket.getEpostDel());

                paket = sorter(epostObjekt.getEpostKropp(),
                        paket.getStartVarde(), 12, "Artikelnamn:", paket);
                epostObjekt.setHamtStalle(paket.getEpostDel());

            } else {
                paket = sorter(epostObjekt.getEpostKropp(),
                        paket.getStartVarde(), 12, "Artikelnamn:", paket);
                epostObjekt.setLeverantor(paket.getEpostDel());
            }
            paket = sorter(epostObjekt.getEpostKropp(),
                    paket.getStartVarde(), 14, "Artikelnummer:", paket);
            epostObjekt.setArtikelNamn(paket.getEpostDel());

            paket = sorter(epostObjekt.getEpostKropp(),
                    paket.getStartVarde(), 6, "Vikt1:", paket);
            epostObjekt.setArtikelNummer(paket.getEpostDel());

            paket = sorter(epostObjekt.getEpostKropp(),
                    paket.getStartVarde(), 6, "Vikt2:", paket);
            epostObjekt.setVikt1(paket.getEpostDel().replace("t", ""));

            paket = sorter(epostObjekt.getEpostKropp(),
                    paket.getStartVarde(), 9, "Slutvikt:", paket);
            epostObjekt.setVikt2(paket.getEpostDel().replace("t", ""));

            paket = sorter(epostObjekt.getEpostKropp(),
                    paket.getStartVarde(), 9, "Referens:", paket);
            epostObjekt.setSlutVikt(paket.getEpostDel().replace("t", ""));

            epostDel = epostKropp.substring(paket.getStartVarde(),
                    epostKropp.length());
            epostObjekt.setReferens(epostDel);


            /*
             Vissa email innehåller två eller fler, transaktioner, därav måste
             man kolla om "Datum:" finns två gånger.
             */
            if (epostObjekt.getReferens().contains("Datum:")) {
                String epostKropp2 = epostObjekt.getReferens();
                if (epostKropp2.length() > 6) {
                    int separator = epostKropp2.indexOf("Datum:");
                    if (separator == 0) {
                        /*
                         Tar hand om dubbelemail där det INTE FINNS någon
                         referens inlagd i transaktionen.
                         */

                        Epost dubbelDatum = new Epost();
                        dubbelDatum.setEpostKropp(epostObjekt.getReferens());
                        list.add(dubbelDatum);
                        epostObjekt.setReferens("");
                    } else {
                        /*
                         Tar hand om dubbelemail där det FINNS någon
                         referens inlagd i transaktionen.
                         */
                        epostKropp2 = epostObjekt.getReferens();

                        epostDel = epostKropp2.substring(0, separator);

                        epostObjekt.setReferens(epostDel);
                        epostDel = epostKropp2.substring(separator, epostKropp2.length());

                        Epost dubbelDatum = new Epost();
                        dubbelDatum.setEpostKropp(epostDel);
                        list.add(dubbelDatum);

                    }
//                    System.out.println("DET VAR ETT DUBBELT EPOST HÄR!");
//                    System.out.println(epostKropp);
                }
            }
            if (epostObjekt.getTransaktionsNummer().length() <= 0) {
                System.out.println("Ett mail blev överhoppat.");
                continue;
            }
            try {
                int transaktionsNummer
                        = Integer.parseInt(epostObjekt.getTransaktionsNummer());
                if (!Ints.contains(allTransaktionsNummer, transaktionsNummer)) {
                    komplettEpost.add(epostObjekt);
                }
            } catch (NumberFormatException e) {
                System.out.println(epostObjekt.getTransaktionsNummer());
                e.printStackTrace();
            }
        }
        return komplettEpost;
    }

    public SorteringsPaket sorter(String epostKropp, int startVarde, int langd,
            String separatorNamn, SorteringsPaket sortP) {

        String epostDel;
        int iterator = startVarde;
        for (;; iterator++) {

            int stopVarde = iterator + langd;

            if (stopVarde > epostKropp.length()) {
                continue;
            }
            epostDel = epostKropp.substring(iterator, stopVarde);
            if (epostDel.equals(separatorNamn)) {
                epostDel = epostKropp.substring(startVarde, iterator);

                sortP.setEpostDel(epostDel);
                sortP.setStartVarde(iterator + langd); // stoppvärde/startvärde
                return sortP;

            }
        }
    }

    public void setPaused(boolean value) {
        this.paused = value;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(OutlookReader.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (!paused) {
                try {
                    this.progress.getJLabel1().setText("Steg 1 av 4 - Kontrollerar Outlook-datafil");
                    this.progress.getJLabel3().setText("0");
                    this.progress.getJLabel2().setText("1");
                    this.progress.getProgressBar().setMinimum(0);
                    this.progress.getProgressBar().setMaximum(1);
                    FileInputStream fis = new FileInputStream(Settings.getFilePath());
                    String md5 = DigestUtils.md5Hex(fis);
                    this.progress.getJLabel3().setText("1");
                    if (!Settings.getMD5().equals(md5)) {
                        ArrayList<Epost> saveList = importEmails();
                        this.progress.getJLabel1().setText("Steg 4 av 4 - Sparar email");
                        sql.saveToDatabase(saveList, progress, this.paused);
                        Settings.setMD5(md5);
                        Settings.saveSettings();
                    } else {
                        this.progress.getProgressBar().setValue(1);
                        this.progress.getJLabel1().setText("Outlook-datafil är inte ändrad. Hoppar över övriga steg");
                    }
                    Thread.sleep(2000);
                    this.progress.isDone();
                    this.paused = true;
                } catch (PSTException | IOException ex) {
                    this.paused = true;
                    this.progress.isDone();
                    System.out.println(ex.getMessage());
                    JOptionPane.showMessageDialog(null, "Outlook-datafil kunde inte hittas.\n\r", "Problem", JOptionPane.ERROR_MESSAGE);
                    this.frame.showSettingsForm();
                } catch (Exception ex) {
                    this.paused = true;
                    this.progress.isDone();
                    System.out.println(ex.getMessage());
                    this.frame.showSettingsForm();
                }
            }
        }
    }
}
