/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import forms.Progress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import outlook.Epost;
import outlook.OutlookReader;

/**
 *
 * @author Per
 */
public class SQLite {

    private final String DB_NAME = "deponi.db";
    private Connection con;
    private PreparedStatement stmt;

    OutlookReader outlook = new OutlookReader();

    public SQLite() throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        con = DriverManager.getConnection("jdbc:sqlite:" + DB_NAME);
        con.setAutoCommit(false);
        this.initilizeDatabase();
    }

    private void initilizeDatabase() throws SQLException {
        stmt = con.prepareStatement(
                "CREATE TABLE IF NOT EXISTS email"
                + "(ID INTEGER PRIMARY KEY,"
                + "datum DATE NOT NULL,"
                + "tid VARCHAR(8) NOT NULL,"
                + "transaktionsNummer INTEGER NOT NULL,"
                + "fordon VARCHAR(10) NOT NULL,"
                + "leverantor VARCHAR(10) NOT NULL,"
                + "hamtStalle VARCHAR(10) DEFAULT 'Finns ej',"
                + "artikelNamn VARCHAR(50) NOT NULL,"
                + "artikelNummer VARCHAR(15) NOT NULL,"
                + "vikt1 VARCHAR(6) NOT NULL,"
                + "vikt2 VARCHAR(6)NOT NULL,"
                + "slutVikt VARCHAR(6) NOT NULL,"
                + "referens VARCHAR(100));"
        );
        stmt.executeUpdate();
    }

    public void closeDatabase() {
        try {
            con.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

    public int getLastTransaktionsNummer() {
        try {
            stmt = con.prepareStatement("SELECT transaktionsNummer FROM email ORDER BY transaktionsNummer DESC LIMIT 1");
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                System.out.println(rs.getInt("transaktionsNummer"));
                return rs.getInt("transaktionsNummer");
            }
        } catch (SQLException ex) {
            System.out.println("the fuck");
        }
        System.out.println("0");
        return 0;
    }

    public String[] getUniqueHamtStalle() {
        ArrayList<String> list = new ArrayList<>();
        list.add("<Välj hämtställe>");
        try {
            stmt = con.prepareStatement("SELECT DISTINCT hamtStalle FROM email ORDER BY hamtStalle ASC");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                list.add(rs.getString("hamtstalle"));
            }
        } catch (SQLException ex) {
            System.out.println("hamtStalle " + ex.getMessage());
        }
        String[] returnlist = new String[list.size()];
        for (int i = 0; i < returnlist.length; i++) {
            returnlist[i] = list.get(i);
        }
        return returnlist;
    }

    public int[] getAllTransaktionsNummer() {
        ArrayList<String> list;
        list = new ArrayList<>();
        try {
            stmt = con.prepareStatement("SELECT transaktionsnummer FROM email");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                list.add(rs.getString("transaktionsnummer"));
            }
        } catch (SQLException ex) {
            System.out.println("getAllTransaktionsNummer " + ex.getMessage());
        }
        int[] returnlist = new int[list.size()];
        for (int i = 0; i < returnlist.length; i++) {
            returnlist[i] = Integer.parseInt(list.get(i));
        }
        return returnlist;
    }

    public void saveToDatabase(ArrayList<Epost> list, Progress panel, boolean paused) throws Exception {
        if (paused) {
            return;
        }
        try {
            panel.getProgressBar().setMaximum(list.size());
            panel.getProgressBar().setValue(0);
            panel.getJLabel2().setText(Integer.toString(list.size()));
            int i = 0;
            for (Epost email : list) {
                if (paused) {
                    con.commit();
                    return;
                }
                stmt = con.prepareStatement("INSERT INTO email "
                        + "(datum, tid, transaktionsNummer, fordon, leverantor, hamtStalle, artikelNamn, artikelNummer, "
                        + "vikt1, vikt2, slutVikt, referens)"
                        + ""
                        + "VALUES "
                        + "(?,?,?,?,?,?,?,?,?,?,?,?)");
                i++; // Iterator för Progress
                stmt.setString(1, email.getDatum());
                stmt.setString(2, email.getTid());
                stmt.setInt(3, Integer.parseInt(email.getTransaktionsNummer()));
                stmt.setString(4, email.getFordon());
                stmt.setString(5, email.getLeverantor());
                stmt.setString(6, email.getHamtStalle());
                stmt.setString(7, email.getArtikelNamn());
                stmt.setString(8, email.getArtikelNummer());
                stmt.setString(9, email.getVikt1());
                stmt.setString(10, email.getVikt2());
                stmt.setString(11, email.getSlutVikt());
                stmt.setString(12, email.getReferens());

                try {
                    stmt.executeUpdate();
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                    Logger.getLogger(SQLite.class.getName()).log(Level.SEVERE, null, e);
                }

                panel.getProgressBar().setValue(i);
                panel.getJLabel3().setText(Integer.toString(i));
            }
            con.commit();
        } catch (SQLException ex) {
            try {
                con.rollback();
                ex.printStackTrace();
                throw new Exception("Något gick fel vid sparningen av epost.");

            } catch (SQLException ex1) {
                System.out.println(ex1.getMessage());
                Logger.getLogger(SQLite.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }

    }

    public ArrayList<Epost> getList(String from, String to, String hamtStalle) throws SQLException {
        ArrayList<Epost> list = new ArrayList<>();
        if (hamtStalle == null) {
            stmt = con.prepareStatement("SELECT * FROM email WHERE datum BETWEEN date(?) AND date(?) ORDER BY datum DESC, tid DESC");
            stmt.setString(1, from);
            stmt.setString(2, to);
        } else {
            stmt = con.prepareStatement("SELECT * FROM email WHERE hamtStalle = ? AND datum BETWEEN date(?) AND date(?) ORDER BY datum DESC, tid DESC, leverantor DESC");
            stmt.setString(1, hamtStalle);
            stmt.setString(2, from);
            stmt.setString(3, to);
        }
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            Epost epost = new Epost();
            epost.setDatum(rs.getString("datum"));
            epost.setTid(rs.getString("tid"));
            epost.setFordon(rs.getString("fordon"));
            epost.setTransaktionsNummer(rs.getString("transaktionsnummer"));
            epost.setLeverantor(rs.getString("leverantor"));
            epost.setHamtStalle(rs.getString("hamtStalle"));
            epost.setArtikelNamn(rs.getString("artikelNamn"));
            epost.setArtikelNummer(rs.getString("artikelNummer"));
            epost.setVikt1(rs.getString("vikt1"));
            epost.setVikt2(rs.getString("vikt2"));
            epost.setSlutVikt(rs.getString("slutVikt"));
            epost.setReferens(rs.getString("referens"));
            list.add(epost);
        }
        return list;
    }
}
