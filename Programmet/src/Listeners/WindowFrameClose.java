package Listeners;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JOptionPane;

/**
 *
 * @author Per
 */
public class WindowFrameClose implements WindowListener {

    private String[] options = {"Ja", "Nej"};

    @Override
    public void windowClosing(WindowEvent we) {
        //Display confirm dialog 
        int confirmed = JOptionPane.showOptionDialog(null,
                "Är du säger på att du vill avsluta?", "Bekräfta avstängning",
                JOptionPane.YES_NO_OPTION, 0, null, options, options[1]);
        
        if (confirmed == JOptionPane.YES_OPTION) {
            //Close frame 
            System.exit(0);
        }
    }

    @Override
    public void windowOpened(WindowEvent we) {
    }

    @Override
    public void windowClosed(WindowEvent we) {
    }

    @Override
    public void windowIconified(WindowEvent we) {
    }

    @Override
    public void windowDeiconified(WindowEvent we) {
    }

    @Override
    public void windowActivated(WindowEvent we) {
    }

    @Override
    public void windowDeactivated(WindowEvent we) {
    }
}
