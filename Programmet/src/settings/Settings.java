package settings;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 *
 * @author svph1344
 */
public final class Settings {

    private static final String configname = "config.properties";
    private static Properties prop = new Properties();

    /**
     * Sparar ner inställningar till fil.
     *
     * @param prop Inställningsfil som skall sparars.
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void saveSettings() throws FileNotFoundException, IOException {
        System.out.println("save ettings");
        OutputStream output = null;
        output = new FileOutputStream(Settings.configname);
        Settings.prop.store(output, null);
    }

    /**
     * Laddar in inställningar från fil.
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void loadSettings() throws FileNotFoundException, IOException {
        Settings.prop = new Properties();
        InputStream input = null;
        input = new FileInputStream(Settings.configname);
        Settings.prop.load(input);
        input.close();
    }

    public static void setFilePath(String path) {
        Settings.prop.setProperty("filepath", path);
    }

    public static void setEmailSubject(String subject) {
        Settings.prop.setProperty("emailsubject", subject);
    }

    public static void setMD5(String md5) {
        Settings.prop.setProperty("fileMD5", md5);
    }

    public static String getMD5() {
        return Settings.prop.getProperty("fileMD5", "");
    }

    public static String getFilePath() {
        return Settings.prop.getProperty("filepath", null);
    }

    public static String getEmailSubject() {
        return Settings.prop.getProperty("emailsubject", null);
    }
}
