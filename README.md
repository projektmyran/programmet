Användningsmanual för Projekt Myran

#Introduktion
Projekt Myran är en körbar prototyp som tillåter användaren att importera transaktioner 
från Outlook-filer, sortera dessa enligt tycke och exportera resultatet till ett Excel-ark. 
Syftet är att automatisera och snabba upp en manuell arbetsuppgift. 

#Export av filer från Outlook
- Klicka på fliken Arkiv
- Klicka på Alternativ
- Klicka på Avancerat
- Klicka på Exportera under Exportera
- Klicka på Exportera till en fil och sedan på Nästa
- Klicka på Outlook-datafil (.pst) och sedan på Nästa
- Välj kontot som du vill exportera (en mapp på den översta nivån). När du gör det kan du exportera alla e-postmeddelanden, kalendern, kontakter, uppgifter och anteckningar om dessa objekt är tillgängliga för det kontot.

Ytterligare information finns på Microsoft Offices hemsida.
https://support.office.com/sv-se/article/Exportera-Outlook-objekt-till-en-Outlook-datafil-pst-14252b52-3075-4e9b-be4e-ff9ef1068f91?ui=sv-SE&rs=sv-SE&ad=SE

#Inläsning av .pst-filer (från Outlook)
Om det rör filer med stort antal epost kan inläsning ta några minuter. Därför är det 
rekommenderat att exportera endast en vecka eller en månad i taget.

#Exportering av .xls-filer (till Excel)
Vid sökning av deponiprojekt väljs det utifrån kod i rullgardinsmenyn under ”Arbetsprojekt”. Det går även att markera rullgardinsmenyn och sedan manuellt skriva  projektnummer. 

Då det vid invägning kan bli felaktig inrapportering av information löses detta av verksamheten genom att skicka ytterligare en transaktion. Prototypen kan dock inte identifiera den korrekta transaktionen. Istället markeras duplikat med röd färg i Excel-filen, varefter korrekt transaktion manuellt får justeras.

#Hur lagring fungerar i Projekt Myran
SQLite, ett tredjepartsbibliotek, används för att lagra information i en .db-fil. Detta innebär 
att databasfilen är beständig och all inläst epost finns kvar även vid inläsning av ny .pst-fil.

Vid behov av mer detaljerad sökning kan detta göras genom att ladda ned gratisprogramvaran SQLiteBrowser.

#Vad du behöver för att prototypen ska starta
För användning av prototypen krävs Java Runtime Environment 8 (JRE 1.8). Äldre versioner fungerar inte. Denna programvara finns gratis på Oracles hemsida. 
http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html

#Generell felhantering och problemlösning
Vid fel i prototypen bör i första hand ny .pst-fil skapas och köras. Ifall felet kvarstår bör ni radera config.properties och/eller deponi.db. Glöm inte att ta en säkerhetskopia på viktigt 
material innan. Dessa finns i samma mapp som .jar-filen för prototypen. Ifall felet kvarstår 
även nu rekommenderas en ny installation av prototypen. Glöm inte att ta en säkerhetskopia på viktigt material innan.

#Information kring vidareutveckling av Projekt Myran
Prototypen använder sig av sex tredjepartsbibliotek. Dessa har varierande licenser som ni 
kan vara tvungna att ta hänsyn till om ni väljer att anlita någon för vidareutveckling. 

- JXL [JXL](asd)http://jexcelapi.sourceforge.net/
- SQLite https://www.sqlite.org/copyright.html
- Google Guava https://github.com/google/guava
- Apache Commons https://commons.apache.org/
- DateChooser http://jdatechooser.sourceforge.net/
- Javalib-pst https://github.com/rjohnsondev/java-libpst

Programmet är utvecklat i programspråket Java, kompilerat med JDK 1.8 och det grafiska
gränssnittet är skapat i Swing.

#Teamet bakom Projekt Myran
- Emilio Pérez Iznaga, Projektledare
- Magnus Aronsson, Programmering / Utveckling
- Per Strömgren, Programmering / Utveckling
- Angelica Wendell, Programmering / Utveckling
- Emil Johansson, Programmering / Utveckling
- Rasmus Calner, Programmering / Utveckling
- Cecilia Wännberg, Dokumentation / Formalia
- Patrik Sandström, Dokumentation / Formalia